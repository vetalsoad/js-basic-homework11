"use strict";

// як це зробити краще?) привіт)))

const togglePasswordIcons = document.querySelectorAll(".icon-password");

// змінюємо іконку та показуємо пароль
function togglePassword(event) {
  const input = event.target.previousElementSibling;
  const icon = event.target;

  if (input.type === "password") {
    input.type = "text";
    icon.classList.remove("fa-eye");
    icon.classList.add("fa-eye-slash");
  } else {
    input.type = "password";
    icon.classList.remove("fa-eye-slash");
    icon.classList.add("fa-eye");
  }
}

togglePasswordIcons.forEach((icon) => {
  icon.addEventListener("click", togglePassword);
});

const btn = document.querySelector(".btn");
const passwordInputs = document.querySelectorAll("input[type='password']");

// зрівняємо паролі
function comparePasswords(event) {
  event.preventDefault();
  if ((passwordInputs[0].value || passwordInputs[1].value) === "") {
    alert("дуже смішно");
  } else if (passwordInputs[0].value === passwordInputs[1].value) {
    alert("You are welcome");
  } else {
    const p = document.createElement("p");
    p.textContent = "Потрібно ввести однакові значення";
    p.style.color = "red";
    btn.parentNode.insertBefore(p, btn);
  }
}

btn.addEventListener("click", comparePasswords);
